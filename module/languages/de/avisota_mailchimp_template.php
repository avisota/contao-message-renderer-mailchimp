<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:15:58+01:00
 */

$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-1-2']               = '1 Spalte - 2 Spalten';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-1-2-leftsidebar']   = '1 Spalte  - 2 Spalten - Seitenleiste links';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-1-2-rightsidebar']  = '1 Spalte  - 2 Spalten - Seitenleiste rechts';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-2-1']               = '2 Spalten - 1 Spalte';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-2-1-leftsidebar']   = '2 Spalten  - 1 Spalte - Seitenleiste links';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-2-1-rightsidebar']  = '2 Spalten  - 1 Spalte - Seitenleiste rechts';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-basic2column']      = '2 Spalten';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3']               = '1 Spalte - 3 Spalten';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3-asym']          = '1 Spalte - 3 Spalten asymetrisch';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3-leftsidebar']   = '1 Spalte  - 3 Spalten - Seitenleiste links';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3-rightsidebar']  = '1 Spalte  - 3 Spalten - Seitenleiste rechts';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-3-1']               = '3 Spalten - 1 Spalte';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-3-1-asym']          = '3 Spalten asymetrisch - 1 Spalte';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-basic3column']      = '3 Spalten';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-basic3column-asym'] = '3 Spalten asymetrisch';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['templates']              = 'Vorlagen (nicht responsiv)';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['templates-plain']        = 'Vorlagen (einfacher Text)';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['templates-responsive']   = 'Vorlagen (responsiv)';
