<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-renderer-mailchimp
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Mailchimp templates
 */
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['templates']              = 'Templates (non-responsive)';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-1-2']               = '1 column - 2 columns';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-1-2-leftsidebar']   = '1 column - 2 columns - sidebar left';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-1-2-rightsidebar']  = '1 column - 2 columns - sidebar right';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-2-1']               = '2 columns - 1 column';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-2-1-leftsidebar']   = '2 columns - 1 column - sidebar left';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-2-1-rightsidebar']  = '2 columns - 1 column - sidebar right';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-basic2column']      = '2 columns';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3']               = '1 column - 3 columns';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3-asym']          = '1 column - 3 columns asymmetric';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3-leftsidebar']   = '1 column - 3 columns - sidebar left';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3-rightsidebar']  = '1 column - 3 columns - sidebar right';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-3-1']               = '3 columns - 1 column';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-3-1-asym']          = '3 columns asymmetric - 1 column';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-basic3column']      = '3 columns';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-basic3column-asym'] = '3 columns asymmetric';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['transactional-basic']    = 'Transactional';

$GLOBALS['TL_LANG']['avisota_mailchimp_template']['templates-responsive'] = 'Templates (responsive)';

$GLOBALS['TL_LANG']['avisota_mailchimp_template']['templates-plain'] = 'Templates (plain text)';
