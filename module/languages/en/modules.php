<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-message-renderer-mailchimp
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Module
 */
$GLOBALS['TL_LANG']['MOD']['avisota-message-renderer-mailchimp'] = array(
	'Avisota - Message renderer "MailChimp"',
	'Message renderer for Avisota that use MailChimp templates.'
);
