<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-renderer-mailchimp
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_layout']['mailChimpTemplate'] = array(
    'Mailchimp template',
    'Chose the basic structure and template for this layout.'
);

/**
 * Reference
 */
$GLOBALS['TL_LANG']['orm_avisota_layout']['mailChimp'] = 'MailChimp Blueprints';
