<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:15:59+01:00
 */

$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-1-2']               = '1 colonna - 2 colonnas';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-1-2-leftsidebar']   = '1 colonna - 2 colonnas - trav laterala sanester';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-1-2-rightsidebar']  = '1 colonna - 2 colonnas - trav laterala dretg';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-2-1']               = '2 colonnas - 1 colonna';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-2-1-leftsidebar']   = '2 colonnas - 1 colonna - trav laterala sanester';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-2-1-rightsidebar']  = '2 colonnas - 1 colonna - trav laterala dretg';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['2col-basic2column']      = '2 colonnas';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3']               = '1 colonna - 3 colonnas';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3-asym']          = '1 colonna - 3 colonnas asimetric';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3-leftsidebar']   = '1 colonna - 3 colonnas - trav laterala sanester';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-1-3-rightsidebar']  = '1 colonna - 3 colonnas - trav laterala dretg';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-3-1']               = '3 colonnas - 1 colonna';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-3-1-asym']          = '3 colonnas asimetric - 1 colonna';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-basic3column']      = '3 colonnas';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['3col-basic3column-asym'] = '3 colonnas asimetirc';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['templates']              = 'Templates (betg responsive)';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['templates-plain']        = 'Templates (text betg formatà)';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['templates-responsive']   = 'Templates (responsiv)';
$GLOBALS['TL_LANG']['avisota_mailchimp_template']['transactional-basic']    = 'Transactional';

