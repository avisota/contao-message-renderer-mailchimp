<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link http://help.transifex.com/intro/translating.html
 * @link https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:16:01+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_layout']['mailChimp']              = 'Blueprints da MailChimp';
$GLOBALS['TL_LANG']['orm_avisota_layout']['mailChimpTemplate']['0'] = 'Template da Mailchimp';
$GLOBALS['TL_LANG']['orm_avisota_layout']['mailChimpTemplate']['1'] = 'Tscherna la structura da basa ed il template per quest layout.';

